<?php

namespace App\Controllers;

use CodeIgniter\HTTP\Request;

class Bakul extends BaseController
{
    public function index()
    {
        $_SESSION['cart'] = [
            'items' => [
                ['id' => 1, 'nama' => 'Lastik', 'harga' => '10.99', 'qty' => 2],
                ['id' => 2, 'nama' => 'Blue Toy Car', 'harga' => '15.95', 'qty' => 2],
                ['id' => 3, 'nama' => 'Plastic Toy Gun', 'harga' => '12.99', 'qty' => 2],
                ['id' => 4, 'nama' => 'RC Car', 'harga' => '24.45', 'qty' => 2],
                ['id' => 5, 'nama' => 'AK 98 Toy Gun', 'harga' => '35.45', 'qty' => 2],
            ]
        ];

        return view('bakul.php');
    }
}
