<?php

namespace App\Controllers;

use CodeIgniter\HTTP\Request;

class Produk extends BaseController
{
    var $produk_img_location = 'img/produk/';

    public function __construct()
    {
        $this->session = \Config\Services::session();
        $this->produk_model = new \App\Models\ProdukModel();
    }

    public function homepage()
    {

        $data = [
            'produk' => $this->produk_model->orderBy('id', 'desc')->paginate(10),
            'pager' => $this->produk_model->pager,
            'produk_img_location' => $this->produk_img_location
        ];

        return view('produk_homepage', $data);
    }

    public function index()
    {

        $data = [
            'produk' => $this->produk_model->orderBy('id', 'desc')->paginate(10),
            'pager' => $this->produk_model->pager
        ];

        return view('admin_produk/listing', $data);
    }

    public function edit($id)
    {
        $produk = $this->produk_model->find($id);

        return view('admin_produk/edit', [
            'produk' => $produk,
            'produk_img_location' => $this->produk_img_location
        ]);
    }

    public function slug($slug)
    {
        $produk = $this->produk_model->where('slug', $slug)->first();

        return view('admin_produk/edit', [
            'produk' => $produk,
            'produk_img_location' => $this->produk_img_location
        ]);
    }

    public function save_edit($id)
    {


        $data = [
            'nama' => $this->request->getPost('nama'),
            'harga' => $this->request->getPost('harga'),
            'keterangan' => $this->request->getPost('keterangan')
        ];

        $file = $this->request->getFile('gambar');

        //dd($file);

        //grab the file by name given in HTML form
        if ($file->isReadable()) {

            //Generate a new secure name
            $gambar = $file->getRandomName();

            //Move the file to its new home
            $file->move($this->produk_img_location, $gambar);

            $data['gambar'] = $gambar;
        }

        $this->produk_model->update($id, $data);

        $_SESSION['success'] = true;
        $this->session->markAsFlashdata('success');

        return redirect()->to('/produk/edit/' . $id);
    }

    public function add()
    {
        helper('form');
        return view('admin_produk/add');
    }

    //untuk save data dari add new form
    public function save_new()
    {
        $data = [
            'nama' => $this->request->getPost('nama'),
            'harga' => $this->request->getPost('harga'),
            'keterangan' => $this->request->getPost('keterangan')
        ];

        $file = $this->request->getFile('gambar');

        //dd($file);

        //grab the file by name given in HTML form
        if ($file->isReadable()) {

            //Generate a new secure name
            $gambar = $file->getRandomName();

            //Move the file to its new home
            $file->move($this->produk_img_location, $gambar);

            $data['gambar'] = $gambar;
        }

        $this->produk_model->insert($data);

        $_SESSION['success'] = true;
        $this->session->markAsFlashdata('success');

        return redirect()->to('/produk');
    }

    public function delete($id)
    {
        $this->produk_model->where('id', $id)->delete();

        $_SESSION['deleted'] = true;
        $this->session->markAsFlashdata('deleted');

        return redirect()->back();
    }
}
