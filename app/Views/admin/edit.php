<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit New Gambar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/admin.css">
</head>

<body>
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="/img/logo.png" alt="" width="30" height="30" class="d-inline-block align-text-top">
                KedaiKu
            </a>
        </div>
    </nav>

    <div class="container mt-5">

        <?php if (isset($_SESSION['success'])) : ?>
            <div class="row">
                <div class="col">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Success!</strong> A new data has been updated.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col">
                <h3> <a href="/gambar" class="btn btn-primary">Back</a> Edit Gambar</h3>
            </div>
        </div>
        <hr>
        <form action="/gambar/edit/<?= $gambar['id']; ?>" method="post" enctype="multipart/form-data">
            <?= csrf_field(); ?>
            <div class="row g-3 align-items-center mt-1">
                <div class="col-2">
                    <label for="nama" class="col-form-label">Nama</label>
                </div>
                <div class="col-4">
                    <input type="text" id="nama" name="nama" class="form-control" aria-describedby="nameHelpInline" value="<?= $gambar['nama']; ?>">
                </div>
                <div class="col-4">
                    <span id="nameHelpInline" class="form-text">
                        Sila masukkan nama produk.
                    </span>
                </div>
            </div>

            <div class="row g-3 align-items-center mt-1">
                <div class="col-2">
                    <label for="description" class="col-form-label">Description</label>
                </div>
                <div class="col-4">
                    <textarea class="form-control" placeholder="Sila masukkan deskripsi produk disini" id="keterangan" name="keterangan" style="height: 100px"><?= $gambar['keterangan']; ?></textarea>
                </div>
                <div class="col-4">
                    &nbsp;
                </div>
            </div>

            <div class="row g-3 align-items-center mt-1">
                <div class="col-2">
                    <label for="nama_fail" class="col-form-label">Gambar</label>
                </div>
                <img src="/img/<?= $gambar['nama_fail']; ?>" alt="" class="img-fluid" style="max-width:300px;">
                <div class="col-4">
                    <input class="form-control" type="file" id="nama_fail" name="nama_fail">
                </div>
                <div class="col-4">
                    <span class="form-text">
                        Sila masukkan hanya file berformat .jpg, jpeg dan .png
                    </span>
                </div>
            </div>

            <div class="row g-3 align-items-center mt-1">
                <div class="col-2">
                    &nbsp;
                </div>
                <div class="col-4">
                    <button class="btn btn-primary" type="submit">Update</button>
                </div>
            </div>


        </form>

    </div>

    <footer class="text-center p-5">
        <p>Hakcipta Terpelihara &copy; 2021</p>
    </footer>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</html>