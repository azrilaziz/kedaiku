<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homepage</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>

    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="/img/logo.png" alt="" width="30" height="30" class="d-inline-block align-text-top">
                KedaiKu
            </a>
        </div>
    </nav>

    <div class="hero-area">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1>Kedaiku</h1>
                    <p>Kedai online anda.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">

        <div class="row">
            <div class="col text-center">
                <h3>Produk</h3>
            </div>
        </div>

        <div class="row">

            <?php foreach ($produk as $p) : ?>

                <div class="col col-sm-6 col-md-4 col-lg-3 mt-3">
                    <div class="card">
                        <img src="<?php echo '/' . $produk_img_location . $p['gambar']; ?>" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5><?php echo $p['nama']; ?></h5>
                            <p class="card-text"><?php echo $p['keterangan']; ?></p>
                            <p><strong>Harga : </strong>RM <?php echo number_format($p['harga'], 2); ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>




        </div>

        <div class="row mt-5">
            <div class="col">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

    </div>

    <footer class="text-center p-5">
        <p>Hakcipta Terpelihara &copy; 2021</p>
    </footer>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</html>