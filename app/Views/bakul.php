<?= $this->extend('templates/front_layout'); ?>

<?= $this->section('main-content'); ?>

<div class="row">
    <div class="col-12">
        <h2><a href="/" class="btn btn-sm btn-primary">Back</a> Shopping Cart</h2>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th> </th>
                    <th>Product</th>
                    <th>Price</th>
                    <th width="8%">Quantity</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>

                <?php if (isset($_SESSION['cart']['items']) && (count($_SESSION['cart']['items']) > 0)) : ?>
                    <?php $counter = 0; ?>
                    <?php $total_amount = 0; ?>
                    <?php foreach ($_SESSION['cart']['items'] as $item) : ?>
                        <tr>
                            <td><?= ++$counter; ?></td>
                            <td><?= $item['nama']; ?></td>
                            <td><?= number_format($item['harga'], 2); ?></td>
                            <td><input type="number" step="1" value="<?= $item['qty']; ?>" class="form-control"></td>
                            <td>RM <?= number_format($item['harga'] * $item['qty'], 2); ?></td>
                        </tr>
                        <?php $total_amount += ($item['harga'] * $item['qty']); ?>
                    <?php endforeach; ?>
                <?php else : ?>

                    <tr>
                        <td colspan="5">Your cart is empty</td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td colspan="4" align="right"><strong> Total Amount</strong></td>
                    <td><strong>RM <?= number_format($total_amount, 2); ?></strong></td>
                </tr>
            </tbody>
        </table>

        <a href="/checkout" class="btn btn-primary float-right">Checkout</a>
    </div>
</div>

<?= $this->endSection(); ?>