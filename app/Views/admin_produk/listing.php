<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Senarai Produk</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/admin.css">
</head>

<body>
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="/img/logo.png" alt="" width="30" height="30" class="d-inline-block align-text-top">
                KedaiKu
            </a>
        </div>
    </nav>

    <div class="container mt-5">

        <?php if (isset($_SESSION['success'])) : ?>
            <div class="row">
                <div class="col">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Success!</strong> A new data have been added.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (isset($_SESSION['deleted'])) : ?>
            <div class="row">
                <div class="col">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Success!</strong> Data has been deleted.
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-12">
                <a href="/produk/add" class="btn btn-primary btn-sm float-end">Add New</a>
                <h3>Senarai Produk</h3>
            </div>

            <div class="col-12">
                <table class="table table-striped table-hover table-sm">
                    <thead class="table-dark">
                        <tr>
                            <th>&nbsp;</th>
                            <th>Gambar</th>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $counter = 0; ?>
                        <?php foreach ($produk as $g) : ?>
                            <tr>
                                <td><?= ++$counter; ?></td>
                                <?php
                                $gambar_url = '/img/produk/' . $g['gambar'];

                                if (!file_exists('img/produk/' . $g['gambar'])) {
                                    $gambar_url = '/img/produk/default.jpg';
                                }
                                ?>
                                <td><img src="<?= $gambar_url; ?>" alt="" class="gambar-pekan"></td>
                                <td><?= $g['nama'] ?></td>
                                <td>RM <?= number_format($g['harga'], 2) ?></td>
                                <td><a href="/produk/edit/<?= $g['id'] ?>" class="btn btn-sm btn-primary">Edit</a>
                                    <button href="/produk/delete/<?= $g['id'] ?>" class="btn btn-sm btn-danger" onclick="confirm_delete(<?= $g['id']; ?>)">Delete</button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col">
                <div id="my-pagination">
                    <?= $pager->links(); ?>
                    <!-- <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav> -->
                </div>
            </div>
        </div>

    </div>

    <footer class="text-center p-5">
        <p>Hakcipta Terpelihara &copy; 2021</p>
    </footer>

    <script>
        function confirm_delete(id) {
            if (confirm('Are you sure to delete ' + id + '?')) {

                window.location.href = '/produk/delete/' + id;
            }
        }
    </script>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</html>